// Package main is the entry point for binary clock executable.
package main

import (
	"flag"
	"time"

	"gitlab.com/air1bzz/binary_clock/internal/component"
	"gitlab.com/air1bzz/binary_clock/internal/logger"
	"gitlab.com/air1bzz/binary_clock/internal/timeadaptor"
)

var debug bool //nolint:gochecknoglobals

// ButtonPin is the pin that will be used for button.
// Must be set at compile time.
var ButtonPin string //nolint:gochecknoglobals

func main() {
	flag.BoolVar(&debug, "debug", false, "Set debug mode")
	flag.Parse()

	log := logger.New(debug)

	adaptor := component.NewRaspberryPiAdaptor(log)

	strip, err := component.NewLedStripDriver(log, adaptor)
	if err != nil {
		log.Error(err, "failed to create led strip driver")
	}

	err = component.NewBrightnessButton(log, adaptor, ButtonPin, strip)
	if err != nil {
		log.Error(err, "failed to init brightness button")
	}

	timeAdaptor := timeadaptor.New(log, strip)

	for tick := range time.NewTicker(time.Second).C {
		timeAdaptor.Set(tick)
	}

	err = strip.Halt()
	if err != nil {
		log.Error(err, "error while halting led strip")
	}
}
