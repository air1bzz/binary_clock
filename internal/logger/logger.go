// Package logger defines a ready to use logger.
package logger

import (
	"github.com/go-logr/logr"
	"github.com/go-logr/zapr"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// New creates a new debug logger.
func New(debug bool) logr.Logger {
	if !debug {
		return zapr.NewLogger(zap.NewNop())
	}

	config := zap.NewDevelopmentConfig()

	config.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	config.EncoderConfig.EncodeTime = zapcore.RFC3339TimeEncoder

	l, err := config.Build()
	if err != nil {
		panic(err)
	}

	return zapr.NewLogger(l)
}
