package component

import (
	"fmt"
	"math/bits"

	"github.com/go-logr/logr"
	"gobot.io/x/gobot/drivers/spi"
)

const defaultBrightness = 31

// LedStripDriver embeds an APA102 driver and its configuration.
type LedStripDriver struct {
	log         logr.Logger
	SecondsSize int
	MinutesSize int
	HoursSize   int
	*spi.APA102Driver
}

// NewLedStripDriver instantiates a new led strip driver.
// The inner driver for led strip is an APA102 driver.
func NewLedStripDriver(log logr.Logger, spiConnector spi.Connector) (*LedStripDriver, error) {
	const (
		maxSeconds = 59
		maxMinutes = maxSeconds
		maxHours   = 23
	)

	log = log.WithName("ledStripDriver").V(1)

	log.Info("Creating new led strip driver...")

	var (
		secondsSize = bits.Len(maxSeconds)
		minutesSize = bits.Len(maxMinutes)
		hoursSize   = bits.Len(maxHours)
	)

	ledSize := secondsSize + minutesSize + hoursSize

	driver := &LedStripDriver{
		log:          log,
		APA102Driver: spi.NewAPA102Driver(spiConnector, ledSize, defaultBrightness),
		SecondsSize:  secondsSize,
		MinutesSize:  minutesSize,
		HoursSize:    hoursSize,
	}

	log.Info(
		fmt.Sprintf("With led size of %d", ledSize),
		"seconds", driver.SecondsSize,
		"minutes", driver.MinutesSize,
		"hours", driver.HoursSize,
		"brightness", driver.Brightness(),
	)

	log.Info(fmt.Sprintf("Driver %v created", driver.Name()))

	if err := driver.Start(); err != nil {
		return nil, fmt.Errorf("can't start LED strip: %w", err)
	}

	return driver, nil
}
