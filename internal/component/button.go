package component

import (
	"fmt"

	"github.com/go-logr/logr"
	"gobot.io/x/gobot/drivers/gpio"
)

const maxBrightness = 31

type buttonDriver struct {
	log logr.Logger
	*gpio.ButtonDriver
}

// NewBrightnessButton initializes a new brightness button.
func NewBrightnessButton(log logr.Logger, reader gpio.DigitalReader, pin string, strip *LedStripDriver) error {
	log = log.WithName("brightnessButton").V(1)

	log.Info("Creating new button driver on pin " + pin)

	buttonDriver := &buttonDriver{
		log:          log,
		ButtonDriver: gpio.NewButtonDriver(reader, pin),
	}

	buttonDriver.SetName("Brightness button")
	buttonDriver.DefaultState = 1

	if err := buttonDriver.Start(); err != nil {
		return fmt.Errorf("failed to start %s: %w", buttonDriver.Name(), err)
	}

	log.Info(fmt.Sprintf("%v created", buttonDriver.Name()))

	buttonDriver.setAction(strip)

	return nil
}

func (button *buttonDriver) setAction(strip *LedStripDriver) {
	events := button.Subscribe()

	go func() {
		for event := range events {
			button.log.Info("Receive event",
				"name", event.Name,
				"data", event.Data,
			)

			if event.Name == gpio.ButtonPush {
				setBrightness(strip)
				button.log.Info(fmt.Sprintf("Brightness set to %d", strip.Brightness()))
			}
		}
	}()
}

func setBrightness(strip *LedStripDriver) {
	const (
		brightness25  = 25
		brightnessOff = 0
		brightnessInc = 5
	)

	brightness := strip.Brightness()

	switch {
	case brightness == brightness25: // if brightness is 25, we don't want to increment to 30, set 31 (max).
		strip.SetBrightness(maxBrightness)
	case brightness < maxBrightness:
		strip.SetBrightness(brightness + brightnessInc)
	default:
		strip.SetBrightness(brightnessOff)
	}
}
