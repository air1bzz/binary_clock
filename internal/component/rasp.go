package component

import (
	"fmt"

	"github.com/go-logr/logr"
	"gobot.io/x/gobot/platforms/raspi"
)

// NewRaspberryPiAdaptor returns a new adaptor.
func NewRaspberryPiAdaptor(log logr.Logger) *raspi.Adaptor {
	log = log.WithName("raspberryPiAdaptor").V(1)

	log.Info("Creating new adaptor...")

	adaptor := raspi.NewAdaptor()

	log.Info(fmt.Sprintf("%v adaptor created", adaptor.Name()))

	return adaptor
}
