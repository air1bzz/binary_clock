/*
Package timeadaptor contains the time adapter needed to process the LED strip driver.
It works by setting a new internal time each second and transforming it so that it can be operated by the LED strip.
It converts seconds, minutes and hours into bool arrays in order to activate or deactivate the LEDs by defining a color.
This color is defined using an incremental cursor on an HSL color representation converted into the necessary RGBA
color used by the LED strip.
*/
package timeadaptor

import (
	"fmt"
	"image/color"
	"math/rand"
	"time"

	"github.com/crazy3lf/colorconv"
	"github.com/go-logr/logr"
	"gitlab.com/air1bzz/binary_clock/internal/component"
)

const (
	maxDegrees        = 360
	defaultLightness  = 0.5
	defaultSaturation = 1
)

// TimeAdaptor decompose and converts time to bool arrays.
type TimeAdaptor struct {
	t             time.Time
	log           logr.Logger
	driver        *component.LedStripDriver
	secondsCursor int
	minutesCursor int
	hoursCursor   int
}

// New instantiates a new time adaptor.
func New(log logr.Logger, driver *component.LedStripDriver) *TimeAdaptor {
	return &TimeAdaptor{
		log:    log.WithName("timeAdaptor").V(1),
		driver: driver,

		secondsCursor: rand.Intn(maxDegrees), //nolint:gosec
		minutesCursor: rand.Intn(maxDegrees), //nolint:gosec
		hoursCursor:   rand.Intn(maxDegrees), //nolint:gosec
	}
}

// Set applies given time to adaptor's inner time.
func (timeAdaptor *TimeAdaptor) Set(newTime time.Time) {
	timeAdaptor.secondsCursor++

	if timeAdaptor.t.Minute() != newTime.Minute() {
		timeAdaptor.minutesCursor++
	}

	if timeAdaptor.t.Hour() != newTime.Hour() {
		timeAdaptor.hoursCursor++
	}

	timeAdaptor.t = newTime

	timeAdaptor.draw()
}

func intBinaryToBoolArray(value int, bitSize int) []bool {
	format := fmt.Sprintf("%%0%db", bitSize)

	var (
		intToString = fmt.Sprintf(format, value)
		array       = make([]bool, len(intToString))
	)

	for i, v := range intToString {
		if v == '1' {
			array[len(intToString)-1-i] = true
		}
	}

	return array
}

// BinaryHours returns hours in the range [0-23] to a reversed binary format as []bool.
func (timeAdaptor *TimeAdaptor) BinaryHours() []bool {
	return intBinaryToBoolArray(timeAdaptor.t.Hour(), timeAdaptor.driver.HoursSize)
}

// BinaryMinutes returns minutes in the range [0-59] to a reversed binary format as []bool.
func (timeAdaptor *TimeAdaptor) BinaryMinutes() []bool {
	return intBinaryToBoolArray(timeAdaptor.t.Minute(), timeAdaptor.driver.MinutesSize)
}

// BinarySeconds returns minutes in the range [0-59] to a reversed binary format as []bool.
func (timeAdaptor *TimeAdaptor) BinarySeconds() []bool {
	return intBinaryToBoolArray(timeAdaptor.t.Second(), timeAdaptor.driver.SecondsSize)
}

// HoursColor returns a RGBA color that use an internal incremental cursor.
func (timeAdaptor *TimeAdaptor) HoursColor() color.RGBA {
	if timeAdaptor.hoursCursor >= maxDegrees {
		timeAdaptor.hoursCursor -= maxDegrees
	}

	r, g, b, err := colorconv.HSLToRGB( //nolint:varnamelen
		float64(timeAdaptor.hoursCursor),
		defaultSaturation,
		defaultLightness,
	)
	if err != nil {
		panic(err)
	}

	return color.RGBA{
		R: r,
		G: g,
		B: b,
	}
}

// MinutesColor returns a RGBA color that use an internal incremental cursor.
func (timeAdaptor *TimeAdaptor) MinutesColor() color.RGBA {
	if timeAdaptor.minutesCursor >= maxDegrees {
		timeAdaptor.minutesCursor -= maxDegrees
	}

	r, g, b, err := colorconv.HSLToRGB( //nolint:varnamelen
		float64(timeAdaptor.minutesCursor),
		defaultSaturation,
		defaultLightness,
	)
	if err != nil {
		panic(err)
	}

	return color.RGBA{
		R: r,
		G: g,
		B: b,
	}
}

// SecondsColor returns a RGBA color that use an internal incremental cursor.
func (timeAdaptor *TimeAdaptor) SecondsColor() color.RGBA {
	if timeAdaptor.secondsCursor >= maxDegrees {
		timeAdaptor.secondsCursor -= maxDegrees
	}

	r, g, b, err := colorconv.HSLToRGB( //nolint:varnamelen
		float64(timeAdaptor.secondsCursor),
		defaultSaturation,
		defaultLightness,
	)
	if err != nil {
		panic(err)
	}

	return color.RGBA{
		R: r,
		G: g,
		B: b,
	}
}

func (timeAdaptor *TimeAdaptor) draw() { //nolint:funlen
	timeAdaptor.log.Info(timeAdaptor.t.Format("15:04:05"))

	for i, on := range timeAdaptor.BinarySeconds() {
		var col color.RGBA
		if on {
			col = timeAdaptor.SecondsColor()
		}

		ledPosition := i

		timeAdaptor.driver.SetRGBA(ledPosition, col)
	}

	timeAdaptor.log.Info(
		"seconds",
		"HSL", timeAdaptor.secondsCursor,
		"RGBA", timeAdaptor.SecondsColor(),
		"binary", timeAdaptor.BinarySeconds(),
		"value", timeAdaptor.t.Second(),
	)

	for i, on := range timeAdaptor.BinaryMinutes() {
		var col color.RGBA
		if on {
			col = timeAdaptor.MinutesColor()
		}

		ledPosition := i + timeAdaptor.driver.SecondsSize

		timeAdaptor.driver.SetRGBA(ledPosition, col)
	}

	timeAdaptor.log.Info(
		"minutes",
		"HSL", timeAdaptor.minutesCursor,
		"RGBA", timeAdaptor.MinutesColor(),
		"binary", timeAdaptor.BinaryMinutes(),
		"value", timeAdaptor.t.Minute(),
	)

	for i, on := range timeAdaptor.BinaryHours() {
		var col color.RGBA
		if on {
			col = timeAdaptor.HoursColor()
		}

		ledPosition := i + timeAdaptor.driver.SecondsSize + timeAdaptor.driver.MinutesSize

		timeAdaptor.driver.SetRGBA(ledPosition, col)
	}

	timeAdaptor.log.Info(
		"hours",
		"HSL", timeAdaptor.hoursCursor,
		"RGBA", timeAdaptor.HoursColor(),
		"binary", timeAdaptor.BinaryHours(),
		"value", timeAdaptor.t.Hour(),
	)

	err := timeAdaptor.driver.Draw()
	if err != nil {
		panic(err)
	}
}
