# Binary Clock

The aim of this project is to create a binary clock running with a **Golang** program using the [Gobot][Gobot] library.

It uses :

- a Raspberry Pi
- an addressable LED strip like [this one][LED]
- a push/pull button
- a 220Ω resistor

Driver used for LED strip is an [APA102](doc/apa-102-super-led-specifications-2013-en.pdf).

## Install

You need to enable **SPI connectors** on your Raspberry config.

You will also need to install [Golang](https://go.dev/doc/install) on your machine.

Then set up the `.env` file with your SSH config to your Raspberry Pi.  
And run:

```bash
task start
```

More information about Raspberry and compiling [here][Rasp].

## Wiring

![Wiring](doc/binary_clock_bb.png)

More information at <https://learn.adafruit.com/adafruit-dotstar-leds/python-circuitpython>

[Gobot]: https://gobot.io
[LED]: https://learn.adafruit.com/adafruit-dotstar-leds/overview
[Rasp]: https://github.com/hybridgroup/gobot/tree/master/platforms/raspi
