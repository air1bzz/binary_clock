module gitlab.com/air1bzz/binary_clock

go 1.23.1

require (
	github.com/crazy3lf/colorconv v1.2.0
	github.com/go-logr/logr v1.4.2
	github.com/go-logr/zapr v1.3.0
	go.uber.org/zap v1.27.0
	gobot.io/x/gobot v1.16.0
)

require (
	github.com/gofrs/uuid v4.0.0+incompatible // indirect
	github.com/hashicorp/errwrap v1.0.0 // indirect
	github.com/hashicorp/go-multierror v1.0.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sigurn/crc8 v0.0.0-20160107002456-e55481d6f45c // indirect
	github.com/sigurn/utils v0.0.0-20190728110027-e1fefb11a144 // indirect
	go.uber.org/multierr v1.10.0 // indirect
	periph.io/x/periph v3.6.2+incompatible // indirect
)
